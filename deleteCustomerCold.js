const aws = require('aws-sdk');
const dynamoDb = new aws.DynamoDB.DocumentClient();

const TableName = 'customer-cold-data-dev';

module.exports.delete = async (accountId) => {
  const params = {
    TableName,
    Key: {
      accountId,
    },
    ConditionExpression: 'accountId = :accountId',
    ExpressionAttributeValues: {
      ':accountId': accountId,
    },
  };

  return await dynamoDb.delete(params).promise();
};



