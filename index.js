const deleteBalance = require('./deleteBalanceHistory').delete;
const deleteCustomer = require('./deleteCustomerCold').delete;

async function balance() {
    const paymentAccountId = 'b1d92de6-4061-4f48-bd0f-2e4fd1ae1585',
      date = '2019-03-17T01:55:11.909Z';

    const item = await deleteBalance(
      paymentAccountId,
      date
    );

    console.log(item);
}

async function customer() {
  const accountId = 'd6e9efd9-1a18-4b0b-8a63-19380753bf6f';

  const item = await deleteCustomer(accountId);

  console.log(item);
}

balance()
customer()


