const aws = require('aws-sdk');
const dynamoDb = new aws.DynamoDB.DocumentClient();

const TableName = 'payment-account-balance-histories-dev';

module.exports.delete = async (paymentAccountId, date) => {
  const params = {
    TableName,
    Key: {
      paymentAccountId,
      date,
    },
    ConditionExpression:
      'paymentAccountId = :paymentAccountId',
    ExpressionAttributeValues: {
      ':paymentAccountId': paymentAccountId,
    },
  };

  return await dynamoDb.delete(params).promise();
};



